FastJet
=======

This package builds "FastJet - A Software Package For Jet Finding In pp
And e<sup>+</sup>e<sup>-</sup> Collisions". Using sources downloaded from
fastjet.fr.
