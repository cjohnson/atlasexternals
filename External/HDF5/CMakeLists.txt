# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building HDF5 for the offline/analysis releases.
#

# The name of the package:
atlas_subdir( HDF5 )

# Temporary directory for the build results:
set( _HDF5BuildDir "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HDF5Build" )

# Find the external(s) needed for the build:
find_package( ZLIB REQUIRED )

# Build HDF5:
ExternalProject_Add( HDF5
   PREFIX ${CMAKE_BINARY_DIR}
   URL https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.1/src/hdf5-1.10.1.tar.gz
   URL_MD5 43a2f9466702fb1db31df98ae6677f15
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_HDF5BuildDir}
   -DHDF5_BUILD_CPP_LIB:BOOL=ON
   -DBUILD_SHARED_LIBS:BOOL=ON
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARIES}
   -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIRS}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( HDF5 buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_HDF5BuildDir}/ <INSTALL_DIR>
   COMMENT "Installing HDF5 into the build area"
   DEPENDEES install )
add_dependencies( Package_HDF5 HDF5 )

# Install HDF5:
install( DIRECTORY ${_HDF5BuildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Clean up:
unset( _HDF5BuildDir )
