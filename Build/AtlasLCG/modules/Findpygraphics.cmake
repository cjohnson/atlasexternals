# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# - Simple module to add the pygraphics directory to the python path
#
# It also provides the functions gen_pyqt_resource and gen_pyqt_uic.
#
# The following variables are defined:
#
#   PYGRAPHICS_FOUND
#   PYGRAPHICS_PYTHON_PATH
#   PYGRAPHICS_BINARY_PATH
#
# Commands:
#
#   pyrcc_cmd
#   pyuic_cmd
#
# Can be steered by PYGRAPHICS_ROOT.
#

# First off, we need Python itself:
find_package( PythonInterp QUIET REQUIRED )

# If it was already found, let's be quiet:
if( PYGRAPHICS_FOUND )
   set( pygraphics_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( PYGRAPHICS_ROOT )
   set( _extraPyGrArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the executable:
find_program( pyrcc_EXECUTABLE NAMES pyrcc4 pyrcc5
   PATH_SUFFIXES bin PATHS ${PYGRAPHICS_ROOT}
   ${_extraPyGrArgs} )

# Set the binary path:
get_filename_component( PYGRAPHICS_BINARY_PATH ${pyrcc_EXECUTABLE} PATH )

# Find the python path:
find_path( PYGRAPHICS_PYTHON_PATH NAMES pydot.py PyQt4/__init__.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${PYGRAPHICS_ROOT}
   ${_extraPyGrArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pygraphics DEFAULT_MSG PYGRAPHICS_PYTHON_PATH
   pyrcc_EXECUTABLE )
mark_as_advanced( PYGRAPHICS_FOUND PYGRAPHICS_BINARY_PATH
   PYGRAPHICS_PYTHON_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pygraphics )

# Clean up:
if( _extraPyGrArgs )
   unset( _extraPyGrArgs )
endif()
